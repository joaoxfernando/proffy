// procurar botão - quando clicar, executar uma ação: duplicar campos
// depois de duplicar, colocar na pagina

document.querySelector("#add-time").addEventListener('click', cloneField)

function cloneField() {
    const newFieldContainer = document.querySelector('.schedule-item').cloneNode(true);
    const fields = newFieldContainer.querySelectorAll('input') 
    
    fields.forEach(function(field) {
        field.value = 0;
    })

    document.querySelector('#schedule-items').appendChild(newFieldContainer)

};