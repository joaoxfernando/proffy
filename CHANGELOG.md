# Histórico de alterações

## [Unreleased]

## [v1.0.0] - 2020-08-08
### Adicionado
    - SQLite
    - Quando não tem aula cadastrada, é exibido uma mensagem na página.

### Alterado 
    - Dados ficam salvos diretamente no BD
    - Após salvar uma aula, usuário é redirecionado para a página de aulas já pronta para exibir a aula recém salva.

## [v0.3.0] - 2020-08-06
### Adicionado
    - NodeJS e Nunjucks

### Alterado
    - Alterada a estrutura dos HTML utilizando o Nunjucks e Node.js para controlar o template engine na página de formulário e de aulas.
    - Reestruturação das pastas devido a adição de Node.js ao projeto
    - Link para enviar WhatsApp alterado de <button> para <a class="button"> no arquivo study.html
    - Alterado de .teacher-item footer button para .teacher-item footer .button

## [v0.2.1] - 2020-08-05
### Alterado
    - Arquivo /css/style.css renomeado para /css/main.css
    
## [v0.2.0] - 2020-08-05
### Adicionado
    - Criação e estilização da página de cadastro de aulas.
    - Adicionado funcionalidades de js para formulário em "/scripts/addField.js".
    - Criado arquivos forms.css e header.css para reaproveitar o código.

### Alterado
    - Refatorado os códigos do page-study.css para o header.css e forms.css

## [v0.1.0] - 2020-08-04
### Adicionado
    - Criação e estilização das páginas iniciais e de aulas disponíveis.