[![HTML5](https://img.shields.io/badge/-HTML5-E34F26?style=flat-square&logo=html5&logoColor=white&link=https://github.com/joaoxfernando/proffy)](https://github.com/joaoxfernando/proffy) &nbsp;&nbsp;[![CSS3](https://img.shields.io/badge/-CSS3-1572B6?style=flat-square&logo=css3&link=https://github.com/joaoxfernando/proffy)](https://github.com/joaoxfernando/proffy) &nbsp;&nbsp;[![JavaScript](https://img.shields.io/badge/-JavaScript-black?style=flat-square&logo=javascript&link=https://github.com/joaoxfernando/proffy)](https://github.com/joaoxfernando/proffy) &nbsp;&nbsp;[![NodeJS](https://img.shields.io/badge/-Node.JS-black?style=flat-square&logo=Node.JS&link=https://github.com/joaoxfernando/proffy)](https://github.com/joaoxfernando/proffy)  &nbsp;&nbsp;[![Nunjucks](https://img.shields.io/badge/-Nunjucks-1E4214?style=flat-square&logo=npm&link=https://github.com/joaoxfernando/proffy)](https://github.com/joaoxfernando/proffy)  &nbsp;&nbsp;[![SQLite](https://img.shields.io/badge/-SQLite-black?style=flat-square&logo=SQLite&link=https://github.com/joaoxfernando/proffy)](https://github.com/joaoxfernando/proffy) 

## Índice
- [Sobre](#Sobre)
- [Prévia](#Prévia)
  - [Desktop](#Desktop)
  - [Mobile](#Mobile)
- [Versões](#Versões)
- [Instalação](#Instalação)
- [Licença](#Licença)
- [Contato](#Contato)

## Sobre

O Proffy é um projeto da Next Level Week da Rocketseat que usa as tecnologias HTML, CSS, JavaScript, Node.js, Nunjucks e ~~SQLite~~ (será implementado)

Nele será possível buscar por aulas onlines com professores das mais diversas matérias e escolher aquele que atende melhor as suas necessidades.

O conteúdo do projeto foi ministrado pelo Instrutor [Mayk Brito](https://github.com/maykbrito).

## Prévia

### Desktop
![Página Inicial](https://user-images.githubusercontent.com/31867867/89544187-30366380-d7d8-11ea-822f-bac3bcb62ac2.png)
![Página das aulas](https://user-images.githubusercontent.com/31867867/89544250-43493380-d7d8-11ea-8df5-4df0ca56198c.png)
![Página de formulário](https://user-images.githubusercontent.com/31867867/89544070-1006a480-d7d8-11ea-81b9-ff94ea406fc5.png)


### Mobile
![Página Inicial](https://user-images.githubusercontent.com/31867867/89544430-80152a80-d7d8-11ea-89a9-d134ccbb7fa4.png)

![Página de aulas](https://user-images.githubusercontent.com/31867867/89544551-a5099d80-d7d8-11ea-9889-8153937394e0.png)

![Página de formulário](https://user-images.githubusercontent.com/31867867/89544589-afc43280-d7d8-11ea-818c-6fefedfeab9a.png)

## Versões
Para visualizar todas as versões e alterações, acesse o `README`. 


## Instalação
    # Clonar o repositório
    git clone https://github.com/joaoxfernando/proffy.git

    # Acesse o diretório
    cd proffy

    # Baixar e instalar as dependências
    npm install

    # Iniciar o servidor
    npm run dev

    # Acessar
    Abra seu navegador e acesse o endereço http://localhost:5000/
  
## Licença
Distribuido sob a licença MIT. Veja ``LICENSE`` para mais informações.

## Contato
Para me localizar ou entrar em contato comigo:<br>
[![Linkedin Badge](https://img.shields.io/badge/-LinkedIn-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/joaoxfernando/)](https://www.linkedin.com/in/joaoxfernando)<br>[![Hotmail Badge](https://img.shields.io/badge/-Hotmail-0078D4?style=flat-square&logo=microsoft-outlook&logoColor=white&link=mailto:joaoxfernando@outlook.com)](mailto:joaoxfernando@outlook.com)<br>[![Whatsapp Badge](https://img.shields.io/badge/-Whatsapp-4CA143?style=flat-square&labelColor=4CA143&logo=whatsapp&logoColor=white&link=https://api.whatsapp.com/send?phone=5584999122284&text=Olá%20João%20Fernando,%20vi%20seu%20perfil%20do%20GitHub!)](https://api.whatsapp.com/send?phone=5511979560214&text=Olá%20João%20Fernando,%20vi%20seu%20perfil%20do%20GitHub!)


[![GitHub](https://i.stack.imgur.com/tskMh.png) GitHub](https://github.com/joaoxfernando)<br>
[![GitLab](https://i.imgur.com/0at0Vk6.png) GitLab](https://gitlab.com/joaoxfernando)